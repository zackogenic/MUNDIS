local MUNDIS = {}

local scriptName = "MUNDIS"

MUNDIS.defaultConfig = {
	currcat = 1,
	currloc = 1
}
MUNDIS.config = DataManager.loadConfiguration(scriptName, MUNDIS.defaultConfig)
local lastPid
local function sendtoallplayers(console)
	lastPid = tes3mp.GetLastPlayerId()

	i = 0

	-- while (i < (lastPid + 1))
	-- do
	--	if Players[i] ~= nil and Players[i]:IsLoggedIn() then
	logicHandler.RunConsoleCommandOnPlayer(i, console, true)
	--i = i + 1
	--	end
	--end
end

function MUNDIS.RunConsoleCommandOnObject(consoleCommand, cellDescription, refId, refNumIndex, mpNum)
	tes3mp.InitializeEvent(tableHelper.getAnyValue(Players).pid)
	tes3mp.SetEventCell(cellDescription)
	tes3mp.SetEventConsoleCommand(consoleCommand)
	tes3mp.SetObjectRefId(refId)
	tes3mp.SetObjectRefNumIndex(refNumIndex)
	tes3mp.SetObjectMpNum(mpNum)
	tes3mp.AddWorldObject()
	tes3mp.SendConsoleCommand()
end

customEventHooks.registerHandler("OnPlayerFinishLogin", function(eventStatus, pid)
	currconfig = DataManager.loadData(scriptName)
	logicHandler.RunConsoleCommandOnPlayer(pid, "set currcat to " .. currconfig.currcat)
	logicHandler.RunConsoleCommandOnPlayer(pid, "set currloc to " .. currconfig.currloc)
	logicHandler.RunConsoleCommandOnPlayer(pid, "stopscript aaaa_mundissummonspell")
end)

MUNDIS.summonlocs = { { objectId = "Abandoned Estate", objectCat = 5, objectLoc = 3 }, { objectId = "Addadshashanammu, Shrine", objectCat = 6, objectLoc = 6 }, { objectId = "Ald Sotha, Upper Level", objectCat = 6, objectLoc = 2 }, { objectId = "Ald-ruhn, Guild of Mages", objectCat = 12, objectLoc = 2 }, { objectId = "Ald-ruhn, Manor District", objectCat = 12, objectLoc = 12 }, { objectId = "Ald-ruhn, Temple", objectCat = 12, objectLoc = 14 }, { objectId = "Aleft", objectCat = 7, objectLoc = 9 }, { objectId = "Almurbalarammi, Shrine", objectCat = 6, objectLoc = 28 }, { objectId = "Arkngthand, Hall of Centrifuge", objectCat = 7, objectLoc = 1 }, { objectId = "Arkngthunch-Sturdumz", objectCat = 7, objectLoc = 10 }, { objectId = "Ashalmawia, Shrine", objectCat = 6, objectLoc = 23 }, { objectId = "Ashalmimilkala, Shrine", objectCat = 6, objectLoc = 17 }, { objectId = "Ashunartes, Shrine", objectCat = 6, objectLoc = 10 }, { objectId = "Assalkushalit, Shrine", objectCat = 6, objectLoc = 29 }, { objectId = "Assarnatamat, Shrine", objectCat = 6, objectLoc = 18 }, { objectId = "Assernerairan, Shrine", objectCat = 6, objectLoc = 19 }, { objectId = "Assurdirapal, Shrine", objectCat = 6, objectLoc = 11 }, { objectId = "Assurnabitashpi, Shrine", objectCat = 6, objectLoc = 3 }, { objectId = "Balmora, Guild of Mages", objectCat = 12, objectLoc = 1 }, { objectId = "Balmora, Temple", objectCat = 12, objectLoc = 13 }, { objectId = "Bamz-Amschend, Hearthfire Hall", objectCat = 7, objectLoc = 8 }, { objectId = "Bthanchend", objectCat = 7, objectLoc = 11 }, { objectId = "Bthuand", objectCat = 7, objectLoc = 12 }, { objectId = "Bthungthumz", objectCat = 7, objectLoc = 13 }, { objectId = "Caldera, Ghorak Manor", objectCat = 12, objectLoc = 17 }, { objectId = "Caldera, Guild of Mages", objectCat = 12, objectLoc = 19 }, { objectId = "Dagoth Ur, Outer Facility", objectCat = 7, objectLoc = 3 }, { objectId = "Druscashti, Upper Level", objectCat = 7, objectLoc = 14 }, { objectId = "Dushariran, Shrine", objectCat = 6, objectLoc = 12 }, { objectId = "Ebernanit, Shrine", objectCat = 6, objectLoc = 20 }, { objectId = "Esutanamus, Shrine", objectCat = 6, objectLoc = 24 }, { objectId = "Galom Daeus, Entry", objectCat = 7, objectLoc = 15 }, { objectId = "Ghostgate, Temple", objectCat = 9, objectLoc = 1 }, { objectId = "Holamayan Monastery", objectCat = 9, objectLoc = 3 }, { objectId = "Ibishammus, Shrine", objectCat = 6, objectLoc = 13 }, { objectId = "Ihinipalit, Shrine", objectCat = 6, objectLoc = 30 }, { objectId = "Kaushtarari, Shrine", objectCat = 6, objectLoc = 14 }, { objectId = "Kushtashpi, Shrine", objectCat = 6, objectLoc = 5 }, { objectId = "Maelkashishi, Shrine", objectCat = 6, objectLoc = 31 }, { objectId = "Mournhold, Godsreach", objectCat = 12, objectLoc = 7 }, { objectId = "Mournhold, Great Bazaar", objectCat = 12, objectLoc = 9 }, { objectId = "Mournhold, Plaza Brindisi Dorom", objectCat = 12, objectLoc = 8 }, { objectId = "Mournhold, Royal Palace: Courtyard", objectCat = 12, objectLoc = 6 }, { objectId = "Mudan, Central Vault", objectCat = 7, objectLoc = 16 }, { objectId = "MUNDIS Control Room", objectCat = 9, objectLoc = 6 }, { objectId = "MUNDIS Prison", objectCat = 12, objectLoc = 20 }, { objectId = "Mzanchend", objectCat = 7, objectLoc = 17 }, { objectId = "Nchardahrk", objectCat = 7, objectLoc = 18 }, { objectId = "Nchardumz", objectCat = 7, objectLoc = 19 }, { objectId = "Nchuleft", objectCat = 7, objectLoc = 20 }, { objectId = "Nchuleftingth, Upper Levels", objectCat = 7, objectLoc = 21 }, { objectId = "Nchurdamz, Interior", objectCat = 7, objectLoc = 22 }, { objectId = "Nissintu", objectCat = 6, objectLoc = 34 }, { objectId = "Odrosal, Tower", objectCat = 7, objectLoc = 2 }, { objectId = "Onnissiralis, Shrine", objectCat = 6, objectLoc = 32 }, { objectId = "Ramimilk, Shrine", objectCat = 6, objectLoc = 25 }, { objectId = "Sadrith Mora, Wolverine Hall: Mage's Guild", objectCat = 12, objectLoc = 18 }, { objectId = "Shashpilamat, Shrine", objectCat = 6, objectLoc = 15 }, { objectId = "Shrine of Azura", objectCat = 6, objectLoc = 9 }, { objectId = "Sjorvar Horse-Mouth's House", objectCat = 3, objectLoc = 11 }, { objectId = "Sobitbael Camp, Mal's Yurt", objectCat = 9, objectLoc = 5 }, { objectId = "Tureynulal, Kagrenac's Library", objectCat = 7, objectLoc = 23 }, { objectId = "Tusenend, Shrine", objectCat = 6, objectLoc = 26 }, { objectId = "Ularradallaku, Shrine", objectCat = 6, objectLoc = 21 }, { objectId = "Vivec, Arena Hidden Area", objectCat = 12, objectLoc = 4 }, { objectId = "Vivec, Arena Pit", objectCat = 12, objectLoc = 25 }, { objectId = "Vivec, Foreign Quarter Plaza", objectCat = 12, objectLoc = 15 }, { objectId = "Vivec, Guild of Mages", objectCat = 12, objectLoc = 3 }, { objectId = "Vivec, High Fane", objectCat = 12, objectLoc = 5 }, { objectId = "Vivec, Hlaalu Plaza", objectCat = 12, objectLoc = 21 }, { objectId = "Vivec, Redoran Plaza", objectCat = 12, objectLoc = 23 }, { objectId = "Vivec, St. Delyn Plaza", objectCat = 12, objectLoc = 24 }, { objectId = "Vivec, St. Olms Plaza", objectCat = 12, objectLoc = 16 }, { objectId = "Vivec, Telvanni Plaza", objectCat = 12, objectLoc = 22 }, { objectId = "Yansirramus, Shrine", objectCat = 6, objectLoc = 27 }, { objectId = "Yasammidan, Shrine", objectCat = 6, objectLoc = 22 }, { objectId = "Zaintiraris, Shrine", objectCat = 6, objectLoc = 33 }, { objectId = "Zergonipal, Shrine", objectCat = 6, objectLoc = 16 } }
MUNDIS.summonext = { { extx = -2, exty = 6, objectCat = 1, objectLoc = 1 }, { extx = -3, exty = -2, objectCat = 1, objectLoc = 2 }, { extx = 18, exty = 4, objectCat = 1, objectLoc = 3 }, { extx = 4, exty = -11, objectCat = 1, objectLoc = 5 }, { extx = -2, exty = 2, objectCat = 2, objectLoc = 1 }, { extx = -11, exty = 11, objectCat = 2, objectLoc = 2 }, { extx = 13, exty = -8, objectCat = 2, objectLoc = 3 }, { extx = -2, exty = -9, objectCat = 2, objectLoc = 4 }, { extx = 11, exty = 14, objectCat = 2, objectLoc = 5 }, { extx = 6, exty = -7, objectCat = 2, objectLoc = 6 }, { extx = 0, exty = -7, objectCat = 2, objectLoc = 7 }, { extx = -25, exty = 19, objectCat = 2, objectLoc = 8 }, { extx = -11, exty = 15, objectCat = 3, objectLoc = 1 }, { extx = 7, exty = 22, objectCat = 3, objectLoc = 2 }, { extx = -8, exty = 3, objectCat = 3, objectLoc = 3 }, { extx = -6, exty = -5, objectCat = 3, objectLoc = 4 }, { extx = -8, exty = 16, objectCat = 3, objectLoc = 5 }, { extx = -3, exty = 12, objectCat = 3, objectLoc = 6 }, { extx = 15, exty = -13, objectCat = 3, objectLoc = 7 }, { extx = 15, exty = 5, objectCat = 3, objectLoc = 8 }, { extx = 15, exty = 2, objectCat = 3, objectLoc = 9 }, { extx = 13, exty = 14, objectCat = 3, objectLoc = 10 }, { extx = -9, exty = 5, objectCat = 4, objectLoc = 1 }, { extx = -10, exty = 9, objectCat = 4, objectLoc = 2 }, { extx = -2, exty = 15, objectCat = 4, objectLoc = 3 }, { extx = 9, exty = 6, objectCat = 4, objectLoc = 4 }, { extx = -6, exty = -1, objectCat = 4, objectLoc = 5 }, { extx = 14, exty = 9, objectCat = 4, objectLoc = 6 }, { extx = 0, exty = 14, objectCat = 4, objectLoc = 7 }, { extx = 4, exty = -3, objectCat = 4, objectLoc = 8 }, { extx = 6, exty = 18, objectCat = 4, objectLoc = 9 }, { extx = 9, exty = -7, objectCat = 4, objectLoc = 10 }, { extx = -1, exty = 18, objectCat = 4, objectLoc = 11 }, { extx = -5, exty = -5, objectCat = 5, objectLoc = 1 }, { extx = 10, exty = 1, objectCat = 5, objectLoc = 2 }, { extx = -5, exty = 9, objectCat = 5, objectLoc = 3 }, { extx = -7, exty = -4, objectCat = 6, objectLoc = 1 }, { extx = 6, exty = -5, objectCat = 6, objectLoc = 4 }, { extx = 11, exty = 20, objectCat = 6, objectLoc = 7 }, { extx = 9, exty = -12, objectCat = 6, objectLoc = 8 }, { extx = 6, exty = 21, objectCat = 7, objectLoc = 4 }, { extx = 0, exty = 10, objectCat = 7, objectLoc = 5 }, { extx = 1, exty = 7, objectCat = 7, objectLoc = 6 }, { extx = 9, exty = -10, objectCat = 7, objectLoc = 7 }, { extx = -2, exty = 5, objectCat = 8, objectLoc = 1 }, { extx = 2, exty = -13, objectCat = 8, objectLoc = 2 }, { extx = -1, exty = -3, objectCat = 8, objectLoc = 3 }, { extx = 18, exty = 3, objectCat = 8, objectLoc = 4 }, { extx = 11, exty = 16, objectCat = 8, objectLoc = 5 }, { extx = 13, exty = -1, objectCat = 8, objectLoc = 6 }, { extx = -4, exty = 18, objectCat = 8, objectLoc = 7 }, { extx = 9, exty = 10, objectCat = 8, objectLoc = 8 }, { extx = -22, exty = 17, objectCat = 8, objectLoc = 9 }, { extx = 2, exty = -7, objectCat = 9, objectLoc = 2 }, { extx = -19, exty = 22, objectCat = 9, objectLoc = 4 }, { extx = 10, exty = 13, objectCat = 12, objectLoc = 10 }, { extx = -2, exty = -2, objectCat = 12, objectLoc = 11 }, { extx = 8, exty = 0, objectCat = 12, objectLoc = 26 } }



function MUNDIS.OnPlayerAttribute(eventStatus, pid)
	cellDescription = tes3mp.GetCell(pid)
	--		tes3mp.MessageBox(pid,0, cellDescription ..  ":" .. Players[pid].data.miscellaneous.selectedSpell)
	if tes3mp.GetDrawState(pid) == 2 and Players[pid].data.miscellaneous.selectedSpell == "aa_summonspell" then
		if tes3mp.IsInExterior(pid) == false then
			--	tes3mp.MessageBox(pid,0, tes3mp.GetCell(pid))
			check = get_value_pos(MUNDIS.summonlocs, tes3mp.GetCell(pid))
			if check > -1 then
				cat = MUNDIS.summonlocs[check].objectCat
				loc = MUNDIS.summonlocs[check].objectLoc

				if MUNDIS.config.currcat == cat and MUNDIS.config.currloc == loc then
					--already here
				else
					sendtoallplayers("set currcat to " .. cat)
					sendtoallplayers("set currloc to " .. loc)

					if (cat < 10) then
						catstr = "0" .. cat
					else
						catstr = cat
					end
					if (loc < 10) then
						locstr = "0" .. loc
					else
						locstr = loc
					end
					logicHandler.RunConsoleCommandOnPlayer(pid,
						"mundis_stonechest->additem az_mund_locstone_" .. catstr .. "_" .. locstr .. " 1")
					sendtoallplayers("PlaySound endboom3")

					MUNDIS.config.currcat = cat
					MUNDIS.config.currloc = loc
					DataManager.saveData(scriptName, MUNDIS.config)
				end
			end
			--	return customEventHooks.makeEventStatus(false, false)
		else
			tes3mp.MessageBox(pid, 0, "X:" .. tes3mp.GetExteriorX(pid) .. " Y" .. tes3mp.GetExteriorY(pid))
			check = get_value_posd(MUNDIS.summonext, tes3mp.GetExteriorX(pid), tes3mp.GetExteriorY(pid))
			if (check > -1) then
				cat = MUNDIS.summonext[check].objectCat
				loc = MUNDIS.summonext[check].objectLoc
				if MUNDIS.config.currcat == cat and MUNDIS.config.currloc == loc then
					--already here
				else
					sendtoallplayers("set currcat to " .. cat)
					sendtoallplayers("set currloc to " .. loc)
					sendtoallplayers("PlaySound endboom3")


					MUNDIS.config.currcat = cat
					MUNDIS.config.currloc = loc
					DataManager.saveData(scriptName, MUNDIS.config)
				end
			end
		end
	end
end

customEventHooks.registerValidator("OnPlayerAttribute", MUNDIS.OnPlayerAttribute)
--mundis.tableall = {mundis_switch_aldruhn = {objectid = "mundis_switch_aldruhn",   objectcat = 1,   objectloc = 1   },mundis_switch_balmora = {objectid = "mundis_switch_balmora",   objectcat = 1,   objectloc = 2   },mundis_switch_caldera = {objectid = "mundis_switch_caldera",   objectcat = 2,   objectloc = 1   },mundis_switch_gnisis = {objectid = "mundis_switch_gnisis",   objectcat = 2,   objectloc = 2   },mundis_switch_mournhold = {objectid = "mundis_switch_mournhold",   objectcat = 1,   objectloc = 4   },mundis_switch_side01 = {objectid = "mundis_switch_side01",   objectcat = 3,   objectloc = 8   },mundis_switch_side02 = {objectid = "mundis_switch_side02",   objectcat = 3,   objectloc = 5   },mundis_switch_side03 = {objectid = "mundis_switch_side03",   objectcat = 2,   objectloc = 3   },mundis_switch_side04 = {objectid = "mundis_switch_side04",   objectcat = 2,   objectloc = 7   },mundis_switch_side05 = {objectid = "mundis_switch_side05",   objectcat = 2,   objectloc = 6   },mundis_switch_side06 = {objectid = "mundis_switch_side06",   objectcat = 8,   objectloc = 2   },mundis_switch_side07 = {objectid = "mundis_switch_side07",   objectcat = 8,   objectloc = 9   },mundis_switch_side08 = {objectid = "mundis_switch_side08",   objectcat = 1,   objectloc = 4   },mundis_switch_side09 = {objectid = "mundis_switch_side09",   objectcat = 5,   objectloc = 1   },mundis_switch_side10 = {objectid = "mundis_switch_side10",   objectcat = 5,   objectloc = 2   },mundis_switch_side11 = {objectid = "mundis_switch_side11",   objectcat = 5,   objectloc = 3   },mundis_switch_side12 = {objectid = "mundis_switch_side12",   objectcat = 3,   objectloc = 2   },mundis_switch_side13 = {objectid = "mundis_switch_side13",   objectcat = 8,   objectloc = 3   },mundis_switch_side14 = {objectid = "mundis_switch_side14",   objectcat = 3,   objectloc = 4   },mundis_switch_side15 = {objectid = "mundis_switch_side15",   objectcat = 3,   objectloc = 10   },mundis_switch_side16 = {objectid = "mundis_switch_side16",   objectcat = 3,   objectloc = 1   },mundis_switch_sadrithmora = {objectid = "mundis_switch_sadrithmora",   objectcat = 1,   objectloc = 3   },mundis_switch_seydaneen = {objectid = "mundis_switch_seydaneen",   objectcat = 2,   objectloc = 4   },mundis_switch_vivec = {objectid = "mundis_switch_vivec",   objectcat = 1,   objectloc = 5   },mundis_switch_vos = {objectid = "mundis_switch_vos",   objectcat = 2,   objectloc = 5   }}
MUNDIS.tableall = { { objectId = "mundis_switch_aldruhn", objectCat = 1, objectLoc = 1 }, { objectId = "mundis_switch_balmora", objectCat = 1, objectLoc = 2 }, { objectId = "mundis_switch_caldera", objectCat = 2, objectLoc = 1 }, { objectId = "mundis_switch_gnisis", objectCat = 2, objectLoc = 2 }, { objectId = "mundis_switch_mournhold", objectCat = 1, objectLoc = 4 }, { objectId = "mundis_switch_side01", objectCat = 3, objectLoc = 8 }, { objectId = "mundis_switch_side02", objectCat = 3, objectLoc = 5 }, { objectId = "mundis_switch_side03", objectCat = 2, objectLoc = 3 }, { objectId = "mundis_switch_side04", objectCat = 2, objectLoc = 7 }, { objectId = "mundis_switch_side05", objectCat = 2, objectLoc = 6 }, { objectId = "mundis_switch_side06", objectCat = 8, objectLoc = 2 }, { objectId = "mundis_switch_side07", objectCat = 8, objectLoc = 9 }, { objectId = "mundis_switch_side08", objectCat = 1, objectLoc = 4 }, { objectId = "mundis_switch_side09", objectCat = 5, objectLoc = 1 }, { objectId = "mundis_switch_side10", objectCat = 5, objectLoc = 2 }, { objectId = "mundis_switch_side11", objectCat = 5, objectLoc = 3 }, { objectId = "mundis_switch_side12", objectCat = 3, objectLoc = 2 }, { objectId = "mundis_switch_side13", objectCat = 8, objectLoc = 3 }, { objectId = "mundis_switch_side14", objectCat = 3, objectLoc = 4 }, { objectId = "mundis_switch_side15", objectCat = 3, objectLoc = 10 }, { objectId = "mundis_switch_side16", objectCat = 3, objectLoc = 1 }, { objectId = "mundis_switch_sadrithmora", objectCat = 1, objectLoc = 3 }, { objectId = "mundis_switch_seydaneen", objectCat = 2, objectLoc = 4 }, { objectId = "mundis_switch_vivec", objectCat = 1, objectLoc = 5 }, { objectId = "mundis_switch_vos", objectCat = 2, objectLoc = 5 } }

local tablebutton = { "mundis_switch_aldruhn", "mundis_switch_balmora", "mundis_switch_caldera", "mundis_switch_gnisis",
	"mundis_switch_mournhold", "mundis_switch_side01", "mundis_switch_side02", "mundis_switch_side03",
	"mundis_switch_side04", "mundis_switch_side05", "mundis_switch_side06", "mundis_switch_side07",
	"mundis_switch_side08", "mundis_switch_side09", "mundis_switch_side10", "mundis_switch_side11",
	"mundis_switch_side12", "mundis_switch_side13", "mundis_switch_side14", "mundis_switch_side15",
	"mundis_switch_side16", "mundis_switch_sadrithmora", "mundis_switch_seydaneen", "mundis_switch_vivec",
	"mundis_switch_vos" }
local tablecats = { "1", "1", "2", "2", "1", "3", "3", "2", "2", "2", "8", "8", "1", "5", "5", "5", "3", "8", "3", "3",
	"3",
	"1", "2", "1", "2" }

function MUNDIS.OnObjectActivateValidator(eventStatus, pid, cellDescription, objects, players)
	--tes3mp.MessageBox(pid,0,cellDescription)
	local cellId = tes3mp.GetCell(pid)
	for _, object in pairs(objects) do
		-- Only bothered about the activation if:
		-- > The object is an object (not a player)
		-- > A player is the one doing the activation
		if object.pid == nil and object.activatingPid ~= nil and object.refId ~= nil then
			reprefId = object.refId
			repuniqueIndex = object.uniqueIndex
			object.testvar = "TESTME"
			lastPid = tes3mp.GetLastPlayerId()

			check = get_value_pos(MUNDIS.tableall, reprefId)
			if string.find(reprefId, "az_mund_locstone_") then
				cat = string.sub(reprefId, 18, 19)
				loc = string.sub(reprefId, 21, 22)
				sendtoallplayers("set currcat to " .. cat)
				sendtoallplayers("set currloc to " .. loc)
				sendtoallplayers("PlaySound endboom3")


				MUNDIS.config.currcat = cat
				MUNDIS.config.currloc = loc
				DataManager.saveData(scriptName, MUNDIS.config)
				tes3mp.MessageBox(pid, 0, "loc " .. loc .. " cat " .. cat)
				return customEventHooks.makeEventStatus(false, false)
			end
			if check > -1 then
				cat = MUNDIS.tableall[check].objectCat
				loc = MUNDIS.tableall[check].objectLoc
				sendtoallplayers("set currcat to " .. cat)
				sendtoallplayers("set currloc to " .. loc)
				sendtoallplayers("PlaySound endboom3")


				MUNDIS.config.currcat = cat
				MUNDIS.config.currloc = loc
				DataManager.saveData(scriptName, MUNDIS.config)
				return customEventHooks.makeEventStatus(false, false)
				---		
			end

			--if (LoadedCells[cellId].data.objectData[repuniqueIndex] == nil) then
			--   tes3mp.MessageBox(pid, 0, "This object has no location data -- ")
			--   return customEventHooks.makeEventStatus(true, true)
			-- end
			-- tes3mp.InputDialog(pid, 58332, "What do you want the new name to be?", "")
			--cdesc = cellDescription
		end
		--return customEventHooks.makeEventStatus(true, true)
	end
end

customEventHooks.registerValidator("OnObjectActivate", MUNDIS.OnObjectActivateValidator)

local function has_value(tab, val)
	for index, value in ipairs(tab) do
		if value == val then
			return true
		end
	end

	return false
end
function get_value_pos(tab, val)
	for index, value in ipairs(tab) do
		if value.objectId == val then
			return index
		end
	end

	return -1
end

function get_value_posd(tab, val1, val2)
	for index, value in ipairs(tab) do
		if value.extx == val1 and value.exty == val2 then
			return index
		end
	end

	return -1
end
