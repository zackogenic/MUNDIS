local input = require("openmw.input")
local self = require("openmw.self")
local core = require("openmw.core")
local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local controlEngaged = false
local boxObject
local function onKeyPress(key)
    if key.code == input.KEY.K then
        controlEngaged = not controlEngaged
        core.sendGlobalEvent("setMundisControlState", { state = controlEngaged, player = self.object })
    end
end
local function controlBegin(boxObj)
boxObject = boxObj
camera.setCollisionType(nearby.COLLISION_TYPE.Water)

end
local function onUpdate()
    if controlEngaged then
        local controlData = {}
        controlData.shiftPressed = input.isKeyPressed(input.KEY.LeftShift)
        controlData.tabPressed = input.isKeyPressed(input.KEY.Tab)
core.sendGlobalEvent("controlUpdate",controlData)
    end
end
return {
    engineHandlers = {
        onKeyPress = onKeyPress,
        onUpdate = onUpdate,
    },
    eventHandlers = {
        controlBegin = controlBegin
    }
}
