local I = require("openmw.interfaces")
local world = require("openmw.world")
local core = require("openmw.core")
local util = require("openmw.util")

local controlEngaged = false

local playerRealPosData = {}

local boxObject 
local doorObject
local playerObject

local spinRate = 1

local function createRotation(x, y, z)
    if (core.API_REVISION < 40) then
        return util.vector3(x, y, z)
    else
        local rotate = util.transform.rotateZ((z))
        return rotate
    end
end
local function getAngle(rotation)

    
if core.API_REVISION > 29 then 
    return  rotation:getAnglesZYX()
else
    return  rotation.z
    end

end
local function onLoad(data)
    if not data then return end
    playerRealPosData = data.playerRealPosData
    controlEngaged = data.controlEngaged
end
local function setMundisControlState(data)
    local state = data.state
    playerObject = data.player
    if not state then--dismount
        if not playerRealPosData then
            error("Player POS data missing")
        end
        controlEngaged = false
        playerObject:teleport(playerRealPosData.cellName,playerRealPosData.position,playerRealPosData.rotation)
    else
        local objectData = I.MundisGlobalData.getMundisObjects()
        boxObject = objectData.enterBox
        doorObject = objectData.enterdoor
        if not doorObject.cell.isExterior then
            --Cannot control when not outside
            return
        end
        playerRealPosData = {cellName = playerObject.cell.name,position = playerObject.position,rotation = playerObject.rotation}
        playerObject:teleport(boxObject.cell,boxObject.position,boxObject.rotation)
        playerObject:sendEvent("controlBegin",boxObject)
        controlEngaged = true
    end
end
local function onSave()
    if controlEngaged then
        setMundisControlState(false)
    end
    return { controlEngaged = controlEngaged, playerRealPosData = playerRealPosData }
end
local function setPlayerPosition(pos, rotZ)
        local scr = world.mwscript.getGlobalScript("zhac_positionscr",playerObject)
        scr.variables.px = pos.x
        scr.variables.py = pos.y
        scr.variables.pz = pos.z
        scr.variables.pr = math.deg(rotZ)
        scr.variables.domove = 1
end
local function controlUpdate(keyData)
    if controlEngaged then
        local newPos = boxObject.position

        if keyData.shiftPressed then
            newPos = util.vector3(newPos.x,newPos.y,newPos.z  - 10)
        elseif keyData.tabPressed then
            newPos = util.vector3(newPos.x,newPos.y,newPos.z  + 10)
        end
        setPlayerPosition(newPos,0)
        local boxRot = getAngle(boxObject.rotation)
        boxRot = boxRot + math.rad(spinRate)
        local newRot = createRotation(0,0,boxRot)
        boxObject:teleport(boxObject.cell,newPos,newRot)
        doorObject:teleport(boxObject.cell,newPos,newRot)
    end
end
return {
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad,
        onUpdate = onUpdate
    },
    eventHandlers = {
        setMundisControlState = setMundisControlState,
        controlUpdate = controlUpdate
    }
}
